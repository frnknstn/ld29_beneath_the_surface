#!/usr/bin/python
"""Ludum Dare Base Code

"""

from __future__ import division
from __future__ import print_function

import os
import sys
import random
import time
import itertools

import pygame

from defines import *
from level import Level
from viewport import Viewport
import sprite_types
import resource

class FramerateCounter(object):
    buffer_size = 60
    
    def __init__(self):
        self.rates = [0 for i in range(self.buffer_size)]
        self.average = 0.0
        self.last_time = time.clock()
        self.index = 0
        self.zero_interval_frames = 0
        self.font = pygame.font.SysFont("", 16)


    def frame(self):
        """signal the next frame to the FPS counter, and return the current average framerate"""
        if sys.platform.startswith('linux'):
            this_time = time.time()
        else:
            this_time = time.clock()
        this_interval = (this_time - self.last_time)

        if this_interval == 0:
            # Too short a time between frames, or insufficient timer resolution
            # Don't log this frame
            self.zero_interval_frames += 1
            return self.average

        this_rate = (1 + self.zero_interval_frames) / (this_time - self.last_time)
        self.zero_interval_frames = 0

        # circular buffer for frame rates
        self.index += 1
        if self.index >= self.buffer_size:
            self.index = 0
        self.rates[self.index] = this_rate

        self.last_time = this_time
        self.average = sum(self.rates) / self.buffer_size

        return self.average

    def draw(self, surf):
        surf.blit(self.font.render("FPS: %0.3f" % self.average, True, (0xFF, 0xFF, 0xFF), (0,0,0)), (0, 0))


class Game(object):
    def main(self):
        debug("Game.main() running")

        # set up double-size, if needed
        if not DOUBLE_SIZE:
            screen = pygame.display.set_mode(SCREEN_SIZE)
        else:
            real_screen = pygame.display.set_mode(
                (SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2))
            screen = pygame.Surface(SCREEN_SIZE)

        clock = pygame.time.Clock()

        level = Level("map/test.tmx")
        tiles = level.images

        players = []
        sprites = []        # all sprites and things, including the players
        viewports = []

        pygame.INFO_SPRITE_COUNT = 0

        # prepare the stuff
        #players.append(player)
        #sprites.append(player)
        #sprites.append(sprite_types.SquirrelMob((260,200), "left"))
        
        viewport = Viewport(level, (0, 0), (0, 0), (SCREEN_SIZE[0], SCREEN_SIZE[1]))
        viewport.draw(screen, sprites)
        move = (0, 0)

        viewports.append(viewport)

        pygame.display.flip()

        frame_count = 0
        fps = FramerateCounter()
        info_font = pygame.font.SysFont("", 16)

        # Main Loop
        running = True

        while running:

            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key in (pygame.K_ESCAPE, pygame.K_q):
                        running = False

                    # movement
                    elif event.key == pygame.K_UP:
                        move = (move[0], move[1] - 2)
                    elif event.key == pygame.K_RIGHT:
                        move = (move[0] + 2, move[1])
                        viewport.move_view_by(1, 0)
                    elif event.key == pygame.K_DOWN:
                        move = (move[0], move[1] + 2)
                    elif event.key == pygame.K_LEFT:
                        move = (move[0] - 2, move[1])

                    else:
                        debug(event)

                elif event.type == pygame.KEYUP:
                    # movement
                    if event.key == pygame.K_UP:
                        move = (move[0], move[1] + 2)
                    elif event.key == pygame.K_RIGHT:
                        move = (move[0] - 2, move[1])
                        viewport.move_view_by(1, 0)
                    elif event.key == pygame.K_DOWN:
                        move = (move[0], move[1] - 2)
                    elif event.key == pygame.K_LEFT:
                        move = (move[0] + 2, move[1])


                if event.type == pygame.QUIT:
                    running = False

            # Update all the sprites
            for sprite in sprites:
                sprite.update_animation()

                # do collisions?

            # Update all the viewports
            for viewport in viewports:
                # move viewport if needed
                #viewport.move_view_by(*move)

                viewport.update()
                viewport.draw(screen, sprites, level.triggers)

            frame_count += 1
            fps.frame()

            if DEBUG:
                # draw debug info
                fps.draw(screen)
                info = "%dx%d window, %d viewports, %d sprites (%d drawn), %d frames" % (
                    SCREEN_SIZE[0], SCREEN_SIZE[1], len(viewports), len(sprites), pygame.INFO_SPRITE_COUNT,
                    frame_count
                    )
                screen.blit(info_font.render(info, True, (255,255,255), (0,0,0)), (0, 12))
                pygame.INFO_SPRITE_COUNT = 0


            if DOUBLE_SIZE:
                tmp_surf = pygame.transform.scale(screen, (SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2), real_screen)
                #real_screen.blit(tmp_surf)

            pygame.display.flip()

            if CAP_FPS is not None:
                clock.tick(CAP_FPS)
            else:
                # be a good multitasking program buddy
                time.sleep(0.0005)

        debug("Leaving main loop")
        screen.fill((0, 0, 0))

        pygame.display.flip()


def main():
    try:
        pygame.init()

        resource.load_resources()

        game = Game()
        game.main()

    finally:
        debug("Stopping pygame...")
        pygame.quit()

    debug("Exiting.")


if __name__ == "__main__":
    main()
