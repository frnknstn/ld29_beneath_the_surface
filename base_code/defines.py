#!/usr/bin/python
from __future__ import division
from __future__ import print_function

from pygame.rect import Rect

DEBUG = False
DEBUG = True

RESOURCE_CATEGORIES = ("tiles", "interface", "icons", "music", "sound")

CAP_FPS = 90

DOUBLE_SIZE = False
#DOUBLE_SIZE = True

GAME_STATES = set(("pre-game", "setup", "main", "post-game"))

# interface layout

SCREEN_SIZE = (640, 400)

TILE_SIZE = 32

DIRECTIONS = ("none", "up", "right", "down", "left")

PLAYER_RECTS = (
    Rect((445, 155), (195, 59)),
    Rect((445, 215), (195, 59)),
    Rect((445, 275), (195, 59)),
    Rect((445, 335), (195, 59))
    )

DEFAULT_FONT = "DejaVu Sans"

# functions

def debug(*args):
    if DEBUG:
        print(*args)


if __name__ == "__main__":
    import game
    game.main()
