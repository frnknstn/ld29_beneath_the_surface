#!/usr/bin/python
from __future__ import division
from __future__ import print_function

"""hexgrid.py

framework for a hexagonal map.
"""

import itertools

class Hexagon(object):
    def __init__(self, data):
        self.data = data
        
        self.active = True
    
    def __repr__(self):
        return str(self.data)


class HexGrid(object):
    def __str__(self):
        retval = "Width: %d Height: %d\n" % (self.width, self.height)
        
        odd_row_offset = (len(str(self.data[0][0])) + 1) // 2

        odd = False
        for i, row in enumerate(self.data):
            retval = retval + "%d: " % i
            if odd:
                retval = retval + " " * odd_row_offset
            retval = retval + " ".join(str(cell.data) for cell in row) + "\n"
            odd = not odd
        return retval

    # the tile coord offsets for movement in a direction
    # note that the horiz offset for west directions only applies from odd rows, 
    # and east only from even rows
    deltas = {
        "ne":(1, -1),
        "e":(1, 0),
        "se":(1, 1),
        "sw":(-1, 1),
        "w":(-1, 0),
        "nw":(-1, -1)
        }

    def adjacent_coords(self, coords, direction):
        # get the coords that are adjacent to a particular set of coords, in a 
        # particular direction
        x, y = coords
        dx, dy = self.deltas[direction]

        # hex line truth table for vertical lines
        # even line     0   1   0   1
        # moving right  0   0   1   1
        # use dy?       0   1   1   0
        # XOR           0   1   1   0
        if dy == 0 or ((y % 2 == 0) ^ (dx > 0)):
            x += dx
        y += dy

        return (x, y)
        
    def is_adjacent(self, a, b):
        """"Are the two supplied coord tuples adjacent?"""
        
        ax, ay = a
        bx, by = b
        
        # too far away
        if abs(ax - bx) > 1 or abs(ay - by) > 1:
            return False
        
        # horizontally adjacent if the y is the same
        if abs(ay - by) == 0:
            return True
            
        # vertically adjacent to two cells, depending of if this row is even or
        # odd
        if ax == bx:
            return True
            
        if ay % 2 == 0:
            return (ax - 1) == bx
        else:
            return (ax + 1) == bx

    def from_seq(self, sequence):
        """Set the data in my cells from a supplied 1D sequence"""
        index = 0
        for row in self.data:
            for cell in row:
                cell.data = sequence[index]
                index += 1

    def to_seq(self):
        """convert the data in my cells to a 1D list"""
        retval = []
        for row in self.data:
            for cell in row:
                retval.append(cell.data)

        return retval

    def get_at(self, x, y):
        if x < 0 or y < 0:
            raise IndexError
        return self.data[y][x]

    # iterator support
    def __iter__(self):
        """iterate over my cells in order"""
        def next(self):
            for row in self.data:
                for cell in row:
                    yield cell

        return next(self)


class HexGridFat(HexGrid):
    """A fat hexgrid is one where the left-oriented rows have one more column 
    than the offset rows."""
    def __init__(self, width, height, default=None):
        assert(height > 0)
        assert(width > 1)

        if default is None:
            row_factory = lambda width : [ Hexagon(i) for i in range(width) ]
        else:
            row_factory = lambda width : [ Hexagon(default) for i in range(width) ]

        # make rows
        self.data = []
        for row_num in range(height):
            if row_num % 2 == 0:
                row_width = width
            else:
                row_width = width - 1

            self.data.append(row_factory(row_width))


        # set instance variables
        self.width = width
        self.height = height


class HexGridThin(HexGrid):
    """A thin hexgrid is one where each row has the same number of columns"""
    def __init__(self, width, height, default=None):
        assert(height > 0)
        assert(width > 1)

        if default is None:
            self.data = [[ Hexagon(i) for i in range(width) ] for i in range(height)] 
        else:
            self.data = [[ Hexagon(default) for i in range(width) ] for i in range(height)] 

        self.width = width
        self.height = height


def set_all_active(hexgrid):
    for row in hexgrid.data:
        for cell in row:
            cell.active = True


def main():
    print("Running Hexgrid!")
    hg = HexGridFat(8, 7)
    print(hg)

    # test adjacency
    #l = ( (0,3), (0,4), (1,3), (2, 3), (2, 5), (3, 2), (3, 4), (3, 5))
    l = ( (3, 1), (3, 2), (4, 2), (3, 3) )
    for a, b in itertools.combinations(l, 2):
        if hg.is_adjacent(a, b):
            print("%s is adjacent to %s" % (str(a), str(b)))
        else:
            print("%s is not adjacent to %s" % (str(a), str(b)))
    


if __name__ == "__main__":
    main()
