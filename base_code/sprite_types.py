#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import os
import collections
import random

import pygame

from defines import *

def load_sprite_files():
    """Load sprites from the disk which match the expected format.

    format is spritename_frameset_direction_index.png
    """
    filenames = os.listdir(SPRITE_DIR)
    filenames.sort()

    image_cache = collections.defaultdict(lambda : collections.defaultdict(dict))

    for filename in filenames:
        filepath = os.path.join(SPRITE_DIR, filename)
        if not os.path.isfile(filepath):
            continue

        # check the filename matches our expected format
        file_spritename, file_frameset_name, file_direction, file_index = \
            os.path.splitext(filename)[0].split("_")

        file_index = int(file_index)
        debug("Loading sprite '%s': '%s'-type's %s-facing frame %d of animation %s" % \
              (filename, file_spritename, file_direction, file_index, file_frameset_name))

        # load the sprite
        surf = pygame.image.load(filepath)
        surf.convert()

        # store in our cache
        key = (file_frameset_name, file_direction)
        image_cache[file_spritename][key][file_index] = surf


    # if there are no left frames for a right image, use a flipped right image
    for spritename, cache in image_cache.items():
        for key, frameset in cache.items():
            frameset_name, direction = key
            
            if direction != "right" or (frameset_name, "left") in cache:
                continue

            debug("Flipping %s's right-facing %s animation" % (spritename, frameset_name))
            flipped_frameset = {}
            for index, image in frameset.items():
                flipped_frameset[index] = pygame.transform.flip(image, True, False)

            cache[(frameset_name, "left")] = flipped_frameset

    # store the frames in the main cache
    for spritename in image_cache:
        Sprite.image_cache[spritename] = {}
        for key, value in image_cache[spritename].items():
            Sprite.image_cache[spritename][key] = [ value[x] for x in sorted(value) ]


class Sprite(object):
    """Base class for everything that has a sprite"""
    image_cache = {}

    def __init__(self, spritename, pos):
        self.pos = pos
        self.tile_pos = (self.pos[0] // TILE_SIZE, self.pos[1] // TILE_SIZE)
        self.rect = pygame.Rect(pos, (TILE_SIZE, TILE_SIZE))

        self.anim = None        # our current animation coroutine
        self.image = None
        self.frame_sets = self.image_cache[spritename]

        self.spritename = spritename    # filename prefix used to load sprites


    def update_animation(self):
        """update our animation"""
        if self.anim is None:
            return False

        try:
            retval = self.anim.next()
        except StopIteration:
            debug("Animation on %s ran out!" % repr(self))
            self.anim = None
            retval = False

        return retval


    def get_frames(self, animation_name):
        """Get the most suitable set of frames for the given animation"""
        direction = self.direction

        # find a direction we have images for
        if (animation_name, direction) not in self.frame_sets:
            for priority in ("none", "right", "up", "down", "left"):
                if (animation_name, priority) in self.frame_sets:
                    direction = priority
                    break

        return self.frame_sets[(animation_name, direction)]

    def start_anim(self, anim):
        """Start playing an animation"""
        self.anim = anim
        return anim.next()

    def anim_stand(self):
        """Animation coroutine for standing animation"""
        frames = self.get_frames("stand")
        while True:
            for frame in frames:
                self.image = frame
                yield True

                for delay in range(60):
                    yield False


    def __repr__(self):
        if self.anim is None:
            animation = "None"
        else:
            animation = self.anim.__name__
        return "<%s: %s, 0x%X>" % (self.__class__.__name__, animation, id(self))



class Mob(Sprite):
    """Base class for all mobile sprites"""
    def __init__(self, spritename, pos, direction="none"):
        Sprite.__init__(self, spritename, pos)

        self.hitpoints = 100
        
        self.dx = self.dy = 0.0         # X and Y velocity
        self.set_direction(direction)   # the facing of this sprite
        self.advancing = True           # moving with the flow of triggers, or against?

    def set_direction(self, direction):
        assert direction in DIRECTIONS

        self.direction = direction
        self.start_anim(self.anim_walk())

    def move(self, level):
        """Move us, and do trigger collisions etc. If we are unable to handle a trigger, return it."""

        # move us
        if self.dx == 0 and self.dy == 0:
            return

        old_x, old_y = self.pos
        x, y = old_x + self.dx, old_y + self.dy

        if BENCHMARK:
            # in benchmark mode, bounce the mobs of the edges of the map
            if x <= 0 or x >= level.size[0]:
                self.dx = -self.dx
            if y <= 0 or y >= level.size[1]:
                self.dy = -self.dy

        # check for trigger collisions
        hit_trigger = None
        for trigger in level.triggers:
            if x == trigger.x:
                if ((old_y < trigger.y and y >= trigger.y) or (old_y > trigger.y and y <= trigger.y)):
                    hit_trigger = trigger
                    break

            if y == trigger.y:
                if ((old_x < trigger.x and x >= trigger.x) or (old_x > trigger.x and x <= trigger.x)):
                    hit_trigger = trigger
                    break

        unhandled_trigger = None
        if hit_trigger is not None:
            unhandled_trigger = self.handle_trigger(level, hit_trigger)
            x, y = hit_trigger.level_pos

        if unhandled_trigger is not None:
            print("Unable to handle trigger %s" % trigger.type)

        # do the move
        self.pos = (x, y)
        self.rect.topleft = (round(x), round(y))

        return unhandled_trigger

    def handle_trigger(self, level, trigger):
        """Handle the trigger we just collided with. If not able to handle, return it."""
        retval = trigger

        if trigger.type == "player direction":
            print("It's a player direction:", trigger.direction)

            if trigger.direction == "up":
                self.dx = 0
                self.dy = -PLAYER_SPEED
            elif trigger.direction == "down":
                self.dx = 0
                self.dy = PLAYER_SPEED
            elif trigger.direction == "left":
                self.dx = -PLAYER_SPEED
                self.dy = 0
            elif trigger.direction == "right":
                self.dx = PLAYER_SPEED
                self.dy = 0

            self.set_direction(trigger.direction)

            if not self.advancing:
                self.dx, self.dy = -(self.dx), -(self.dy)

            retval = None

        return retval


    def anim_walk(self):
        """Animation coroutine for walking animation"""
        frames = self.get_frames("walk")
        while True:
            for frame in frames:
                self.image = frame
                yield True

                for delay in range(12):
                    yield False


class PlayerMob(Mob):
    def __init__(self, pos, direction="none"):
        Mob.__init__(self, "player3", pos, direction)
        self.start_anim(self.anim_walk())



class SquirrelMob(Mob):
    def __init__(self, pos, direction="none"):
        Mob.__init__(self, "squirrel", pos, direction)
        self.start_anim(self.anim_walk())

    def anim_stand(self):
        """Animation coroutine for squirrel, start walking again"""
        frames = self.get_frames("stand")

        old_speed = (self.dx, self.dy)
        self.dx = self.dy = 0

        duration = random.randint(120,360)

        for frame in frames:
            self.image = frame
            yield True

        for delay in xrange(duration):
            yield False

        self.dx, self.dy = old_speed
        yield self.start_anim(self.anim_walk())

    def anim_walk(self):
        """Animation coroutine for squirrel, randomly stop."""
        frames = self.get_frames("walk")
        
        for i in range(random.randint(10, 50)):
            for frame in frames:
                self.image = frame
                yield True

                for delay in range(12):
                    yield False

        yield self.start_anim(self.anim_stand())




if __name__ == "__main__":
    import game
    game.main()
