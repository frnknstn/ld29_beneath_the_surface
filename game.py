#!/usr/bin/python
"""Ludum Dare Base Code

"""

from __future__ import division
from __future__ import print_function

import os
import sys
import random
import time
import itertools
import math

import pygame

from defines import *
from level import Level
from viewport import Viewport
import sprite_types
import player_mob
import resource


class FramerateCounter(object):
    buffer_size = 60
    
    def __init__(self):
        self.rates = [0 for i in range(self.buffer_size)]
        self.average = 0.0
        self.last_time = time.clock()
        self.index = 0
        self.zero_interval_frames = 0
        self.font = pygame.font.SysFont("", 16)

    def frame(self):
        """signal the next frame to the FPS counter, and return the current average framerate"""
        if sys.platform.startswith('linux'):
            this_time = time.time()
        else:
            this_time = time.clock()
        this_interval = (this_time - self.last_time)

        if this_interval == 0:
            # Too short a time between frames, or insufficient timer resolution
            # Don't log this frame
            self.zero_interval_frames += 1
            return self.average

        this_rate = (1 + self.zero_interval_frames) / (this_time - self.last_time)
        self.zero_interval_frames = 0

        # circular buffer for frame rates
        self.index += 1
        if self.index >= self.buffer_size:
            self.index = 0
        self.rates[self.index] = this_rate

        self.last_time = this_time
        self.average = sum(self.rates) / self.buffer_size

        return self.average

    def draw(self, surf):
        surf.blit(self.font.render("FPS: %0.3f" % self.average, True, (0xFF, 0xFF, 0xFF), (0,0,0)), (0, 0))


def print_mask(mask):
    size_x, size_y = mask.get_size()

    print(mask)
    for y in range(size_y):
        line = [ str(mask.get_at((x, y))) for x in range(size_x) ]
        print("".join(line))


class Game(object):
    def main(self):
        global frame_count

        debug("Game.main() running")

        # set up double-size, if needed
        if not DOUBLE_SIZE:
            screen = pygame.display.set_mode(SCREEN_SIZE)
        else:
            real_screen = pygame.display.set_mode(
                (SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2))
            screen = pygame.Surface(SCREEN_SIZE)

        clock = pygame.time.Clock()

        level = Level("map/seawolverine.tmx")
        tiles = level.images

        sprite_types.load_sprite_files()

        players = []
        sprites = []        # all sprites and things, including the players
        viewports = []

        pygame.INFO_SPRITE_COUNT = 0

        # prepare the stuff
        player = player_mob.PlayerMob((200, 200), "right", sprites=sprites)
        players.append(player)
        sprites.append(player)

        for i in range(10):
            pos = (random.randint(0, level.size[0]), random.randint(0, level.size[1]))
            direction = random.choice(("left", "right"))
            new_sprite = random.choice((sprite_types.BubblesMob,sprite_types.BibblesMob))(pos, direction)
            new_sprite.set_move_delta((random.random() - 0.5) / 6, 0)
            sprites.append(new_sprite)
        
        viewport = Viewport(level, (0, 0), (0, 0), (SCREEN_SIZE[0], SCREEN_SIZE[1]), player)
        viewport.draw(screen, sprites)
        move = (0, 0)

        viewports.append(viewport)

        pygame.display.flip()

        frame_count = 0
        fps = FramerateCounter()
        info_font = pygame.font.SysFont("", 16)

        # Main Loop
        running = True

        while running:

            # respawn if dead
            if player.dead:
                player.move_to(200, 200)
                player.spawn()
                sprites.append(player)

            # parse events and input
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key in (pygame.K_ESCAPE, pygame.K_q):
                        running = False
                    elif not player.handle_input(event):
                        debug(event)

                elif event.type == pygame.KEYUP:
                    player.handle_input(event)

                elif event.type == pygame.QUIT:
                    running = False

            player.parse_input()
            player.fire_weapons(sprites, frame_count)

            # Update all the sprites
            for sprite in sprites:
                sprite.update_animation()

                # collision detection
                if not sprite.solid:
                    continue

                # tile collisions
                world_collisions = sprite.rect.collidelistall(level.solid_tiles_rects)

                for index in world_collisions:
                    tile_index = level.data[level.solid_tiles[index][0]][level.solid_tiles[index][1]]

                    # do the per-pixel collision checking
                    sprite_mask = pygame.mask.from_surface(sprite.image)
                    tile_mask = level.tile_hitmasks[tile_index]
                    tile_rect = level.solid_tiles_rects[index]

                    if sprite_mask.count() == 0:
                        print("Error: %s has hitmask with no pixels set" % sprite)
                    if tile_mask.count() == 0:
                        print("Error: tile %d has hitmask with no pixels set" % tile_index)

                    offset = (tile_rect.left - sprite.rect.left, tile_rect.top - sprite.rect.top)

                    if sprite_mask.overlap(tile_mask, offset) is not None:
                        sprite.damage(1)

                # other sprite collisions

            # Remove dead sprites
            old_sprites_count = len(sprites)
            sprites = [ x for x in sprites if not x.dead ]
            if len(sprites) != old_sprites_count:
                debug("Removed %d dead sprites" % (old_sprites_count - len(sprites)))

            player.herd_children(sprites)
            self.move_sprites(sprites, level)

            # Update all the viewports
            for viewport in viewports:
                # move viewport if needed
                viewport.move_view_by(*move)

                viewport.update()
                viewport.draw(screen, sprites, level.triggers)

            frame_count += 1
            fps.frame()

            if DEBUG:
                # draw debug info
                fps.draw(screen)
                info = "%dx%d window, %d viewports, %d sprites (%d drawn), %d frames" % (
                    SCREEN_SIZE[0], SCREEN_SIZE[1], len(viewports), len(sprites), pygame.INFO_SPRITE_COUNT,
                    frame_count
                    )
                screen.blit(info_font.render(info, True, (255,255,255), (0,0,0)), (0, 12))
                pygame.INFO_SPRITE_COUNT = 0

            if DOUBLE_SIZE:
                pygame.transform.scale(screen, (SCREEN_SIZE[0] * 2, SCREEN_SIZE[1] * 2), real_screen)

            pygame.display.flip()

            if CAP_FPS is not None:
                clock.tick(CAP_FPS)
            else:
                # be a good multitasking program buddy
                time.sleep(0.0005)

        debug("Leaving main loop")
        screen.fill((0, 0, 0))

        pygame.display.flip()


    def move_sprites(self, sprites, level):

        for sprite in sprites:
            if not isinstance(sprite, sprite_types.Mob):
                continue

            # apply physics - buoyancy
            if sprite.buoyancy != 0:
                old_dy = sprite.dy
                dy = old_dy - sprite.buoyancy
                sprite.set_move_delta(sprite.dx, dy)

            # apply physics - water resistance
            dr = sprite.dr
            if dr != 0 and sprite.mu_coefficient != 0:
                # faster objects have more turbulence
                speed_scale = (dr / WATER_RESISTANCE_SCALE)

                dr -= WATER_RESISTANCE * sprite.mu_coefficient * speed_scale

                if dr < abs(sprite.buoyancy):
                    dr = abs(sprite.buoyancy)
                sprite.set_move_vector(sprite.theta, dr)

            sprite.move(level)


def main():
    try:
        pygame.init()

        resource.load_resources()

        game = Game()
        game.main()

    finally:
        debug("Stopping pygame...")
        pygame.quit()

    debug("Exiting.")


if __name__ == "__main__":
    main()
