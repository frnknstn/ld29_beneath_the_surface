#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import os
import collections
import random
import math

import pygame

from defines import *

def load_sprite_files():
    """Load sprites from the disk which match the expected format.

    format is spritename_frameset_direction_index.png
    """
    filenames = os.listdir(SPRITE_DIR)
    filenames.sort()

    image_cache = collections.defaultdict(lambda : collections.defaultdict(dict))

    for filename in filenames:
        filepath = os.path.join(SPRITE_DIR, filename)
        if not os.path.isfile(filepath):
            continue

        # check the filename matches our expected format
        file_spritename, file_frameset_name, file_direction, file_index = \
            os.path.splitext(filename)[0].split("_")

        file_index = int(file_index)
        debug("Loading sprite '%s': '%s'-type's %s-facing frame %d of animation %s" % \
              (filename, file_spritename, file_direction, file_index, file_frameset_name))

        # load the sprite
        surf = pygame.image.load(filepath)
        surf.convert()

        # store in our cache
        key = (file_frameset_name, file_direction)
        image_cache[file_spritename][key][file_index] = surf


    # if there are no left frames for a right image, use a flipped right image
    for spritename, cache in image_cache.items():
        for key, frameset in cache.items():
            frameset_name, direction = key
            
            if direction != "right" or (frameset_name, "left") in cache:
                continue

            debug("Flipping %s's right-facing %s animation" % (spritename, frameset_name))
            flipped_frameset = {}
            for index, image in frameset.items():
                flipped_frameset[index] = pygame.transform.flip(image, True, False)

            cache[(frameset_name, "left")] = flipped_frameset

    # store the frames in the main cache
    for spritename in image_cache:
        Sprite.image_cache[spritename] = {}
        for key, value in image_cache[spritename].items():
            Sprite.image_cache[spritename][key] = [ value[x] for x in sorted(value) ]


class Sprite(object):
    """Base class for everything that has a sprite"""
    image_cache = {}

    def __init__(self, spritename, pos):
        # set up our animations
        self.anim = None        # our current animation coroutine
        self.image = None
        self.frame_sets = self.image_cache[spritename]

        self.spritename = spritename    # filename prefix used to load sprites

        # sample any of our sprites to determine our size
        if len(self.frame_sets) == 0:
            size =  (TILE_SIZE, TILE_SIZE)
        else:
            size = self.frame_sets.values()[0][0].get_size()

        # set up our positions
        self.pos = pos                                                          # center of the object
        self.tile_pos = (self.pos[0] // TILE_SIZE, self.pos[1] // TILE_SIZE)    # index of the tile we are on
        self.rect = pygame.Rect((0,0), size)                                    # actual sprite's draw rect
        self.rect.center = pos

        self.dead = False       # remove me from sprite lists now
        self.solid = False      # do colision detection


    def update_animation(self):
        """update our animation"""
        if self.anim is None:
            return False

        try:
            retval = self.anim.next()
        except StopIteration:
            debug("Animation on %s ran out!" % repr(self))
            self.anim = None
            retval = False

        return retval


    def get_frames(self, animation_name):
        """Get the most suitable set of frames for the given animation"""
        direction = self.direction

        # find a direction we have images for
        if (animation_name, direction) not in self.frame_sets:
            for priority in ("none", "right", "up", "down", "left"):
                if (animation_name, priority) in self.frame_sets:
                    direction = priority
                    break

        return self.frame_sets[(animation_name, direction)]

    def start_anim(self, anim):
        """Start playing an animation"""
        self.anim = anim
        return anim.next()

    def anim_stand(self):
        """Animation coroutine for standing animation"""
        frames = self.get_frames("stand")
        while True:
            for frame in frames:
                self.image = frame
                yield True

                for delay in range(60):
                    yield False

    def __repr__(self):
        if self.anim is None:
            animation = "None"
        else:
            animation = self.anim.__name__
        return "<%s: %s, 0x%X>" % (self.__class__.__name__, animation, id(self))



class Mob(Sprite):
    """Base class for all mobile sprites"""
    def __init__(self, spritename, pos, direction="none"):
        Sprite.__init__(self, spritename, pos)

        self.hitpoints = 100
        
        self.dx = self.dy = 0.0         # X and Y velocity in pixels / frame
        self.dr = self.theta = 0.0      # velocity vector in pixels / frame

        self.buoyancy = 0.0             # relative buoyancy, > 0 floats and < 0 sinks
        self.mu_coefficient = 1.0       # friction mulitplier for the object

        self.set_direction(direction)   # the facing of this sprite

    def damage(self, damage, source = None):
        """Damage this object, and possibly kill it"""
        if self.dead:
            # can't hurt a dead man
            return

        self.hitpoints -= damage

        if self.hitpoints <= 0:
            self.dead = True

        debug("%s took %0.3f damage from %s. HP now %0.3f" % (str(self), damage, str(source), self.hitpoints));

    def set_move_vector(self, theta, dr):
        """change the mob's velocity vector, as polar coords in pixels / frame"""
        self.theta = theta
        self.dr = dr

        self.dx = dr * math.cos(theta)
        self.dy = (dr * math.sin(theta))

    def set_move_delta(self, dx, dy):
        """change the mob's velocity, as cartesian coords in pixels / frame"""
        self.dx = dx
        self.dy = dy

        self.theta = math.atan2(dy, dx)
        self.dr = math.hypot(dx, dy)

    def set_direction(self, direction):
        assert direction in DIRECTIONS

        self.direction = direction
        self.start_anim(self.anim_walk())

    def move(self, level):
        """Move us one frame's worth"""

        # move us
        if self.dx == 0 and self.dy == 0:
            return

        old_x, old_y = self.pos
        x, y = old_x + self.dx, old_y + self.dy

        if BENCHMARK:
            # in benchmark mode, bounce the mobs of the edges of the map
            if x <= 0 or x >= level.size[0]:
                self.dx = -self.dx
            if y <= 0 or y >= level.size[1]:
                self.dy = -self.dy

        # do the move
        self.move_to(x,y)

    def move_to(self, x, y):
        """Move us to the specified coordinates"""
        self.pos = (x, y)
        self.rect.center = (round(x), round(y))

    def anim_walk(self):
        """Animation coroutine for walking animation"""
        frames = self.get_frames("walk")
        while True:
            for frame in frames:
                self.image = frame
                yield True

                for delay in range(12):
                    yield False


class BubblesMob(Mob):
    def __init__(self, pos, direction="none"):
        Mob.__init__(self, "bubbles", pos, direction)

        self.buoyancy = 0.040
        self.start_anim(self.anim_walk())

    def anim_walk(self):
        """Animation coroutine for waving bubbles"""
        frames = self.get_frames("walk")
        
        # set up the bubble wobble
        angle_max = math.pi * 2
        angle_diff = angle_max / 90 / (1 + random.random())
        offset = 0
        angle = random.random() * angle_max
        scale = 2 + (random.random() * 2)

        while True:
            for frame in frames:
                for i in range(13):
                    x, y = self.pos
                    x -= offset
                    angle += angle_diff
                    if angle > angle_max:
                        angle = 0
                    offset = math.sin(angle) * scale
                    x += offset

                    if y < (0 - self.rect.height):
                        y = 400
                        self.set_move_vector(self.theta, 0)

                    self.move_to(x, y)
                    if i == 0:
                        self.image = frame
                        yield True
                    else:
                        yield False

class BibblesMob(BubblesMob):
    def __init__(self, pos, direction="none"):
        direction = random.choice(("left", "right"))
        random_sprite = random.choice(("bib", "bub"))
        Mob.__init__(self, random_sprite, pos, direction)

        self.buoyancy = 0.040
        self.start_anim(self.anim_walk())


class TinyBibbleMob(BubblesMob):
    def __init__(self, pos, direction="none", vector = None):
        """Temporary bubbles that disappear in less than a second. 

        Direction is ignored
        """
        direction = random.choice(("left", "right"))
        random_sprite = random.choice(("tinybib", "tinybub"))
        Mob.__init__(self, random_sprite, pos, direction)

        # use a simplified wobble
        if vector is None:
            self.set_move_delta(random.triangular(-1, 1), -0.8)
        else:
            self.set_move_vector(*vector)

        self.buoyancy = 0.040
        self.start_anim(self.anim_walk())



    def anim_walk(self):
        """Animation coroutine for waving bubbles"""
        frames = self.get_frames("walk")
        lifespan = 45
        age = 0
        
        while True:
            for frame in frames:
                for i in range(13):
                    if age > lifespan:
                        self.dead = True
                        yield True
                    age += 1

                    if i == 0:
                        self.image = frame
                        yield True
                    else:
                        yield False

class BulletMob(Mob):
    def __init__(self, pos, theta, dr, owner):
        """Bullet mob, with an owner refto keep track of kills, that inherents momentum from it's gun"""
        Mob.__init__(self, "bullet", pos, "right")
        self.owner = owner

        # inherit momentum
        self.set_move_vector(theta + random.triangular(-0.01, 0.01), dr + 5)

        self.hitpoints = 1

        self.solid = True
        self.buoyancy = -0.01
        self.mu_coefficient = 0.8
        self.start_anim(self.anim_shoot())

    def anim_shoot(self):
        """Animation coroutine for bullets"""
        frames = self.get_frames("walk")
        lifespan = 620
        age = 0
        
        while True:
            for frame in frames:
                for i in range(13):
                    if age > lifespan:
                        self.dead = True
                        yield True
                    age += 1

                    if i == 0:
                        self.image = frame
                        yield True
                    else:
                        yield False



class BlobMob(Mob):
    def __init__(self, pos, direction="none", vector = None):
        """Nonmobile mob :)"""
        direction = "right"
        Mob.__init__(self, "bullet", pos, direction)

        self.buoyancy = 0
        self.start_anim(self.anim_walk())

        print(pos, self.rect)

class TestMob(Mob):
    def __init__(self, pos, direction="none"):
        Mob.__init__(self, "bubbles", pos, direction)
        self.start_anim(self.anim_walk())

    def anim_stand(self):
        """Animation coroutine for squirrel, start walking again"""
        frames = self.get_frames("stand")

        old_speed = (self.dx, self.dy)
        self.dx = self.dy = 0

        duration = random.randint(120,360)

        for frame in frames:
            self.image = frame
            yield True

        for delay in xrange(duration):
            yield False

        self.dx, self.dy = old_speed
        yield self.start_anim(self.anim_walk())

    def anim_walk(self):
        """Animation coroutine for squirrel, randomly stop."""
        frames = self.get_frames("walk")
        
        for i in range(random.randint(10, 50)):
            for frame in frames:
                self.image = frame
                yield True

                for delay in range(12):
                    yield False

        yield self.start_anim(self.anim_stand())




if __name__ == "__main__":
    import game
    game.main()
