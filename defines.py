#!/usr/bin/python
from __future__ import division
from __future__ import print_function

from pygame.rect import Rect

DEBUG = False
DEBUG = True

BENCHMARK = False

RESOURCE_CATEGORIES = ("tiles", "interface", "icons", "music", "sound")
SPRITE_DIR = "sprites"
CAP_FPS = None
CAP_FPS = 60
#CAP_FPS = 90
#CAP_FPS = 250

DOUBLE_SIZE = False
DOUBLE_SIZE = True

GAME_STATES = set(("pre-game", "setup", "main", "post-game"))

DRAW_MAP = True
#DRAW_MAP = False

# physics
WATER_RESISTANCE = 0.033
WATER_RESISTANCE_SCALE = 2
# interface layout

SCREEN_SIZE = (640, 400)

TILE_SIZE = 16

DIRECTIONS = ("none", "up", "right", "down", "left")


DEFAULT_FONT = "DejaVu Sans"

# functions

def debug(*args):
    if DEBUG:
        print(*args)


if __name__ == "__main__":
    import game
    game.main()
