#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import random
import math

import pygame

from defines import *

import sprite_types

class PlayerMob(sprite_types.Mob):
    def __init__(self, pos, direction="none", sprites = None):
        sprite_types.Mob.__init__(self, "submarine1", pos, direction)

        self.child_sprites = []

        # assume the sub's propulsion can offset friction
        self.mu_coefficient = 0
        self.buoyancy = 0
        self.solid = True

        # input flags - sub control
        self.accelerating = False
        self.stopping = False
        self.climbing = False
        self.diving = False
        self.gun = False
        self.torpedo = False
        self.mine = False

        # physical location of weapons on sprite
        self.gun_location = (16, 0)
        self.torpedo_location = (10, -5)
        self.mine_location = (-10, -8)
        self.engine_location = (-16, -2)

        # image rotation-related
        self.image_real = self.image
        self.image_heading = 0
        self.image_flipped = False

        self.spawn()


    def spawn(self):
        """Reset our variables to default"""
        self.dead = False
        self.set_move_delta(0, 0)

        # submarine-specific 
        self.engine_speed = 0
        self.acceleration = 0.0139
        self.braking = 0.0100
        self.max_speed = 2.5
        self.hitpoints = 100

        # keep our degree heading separate, as the sprite can go upside down
        self.heading = 0
        self.flipped = False
        self.climb_rate = 3
        self.dive_rate = 2

        # weapon-related
        self.gun_cooldown = 0
        self.torpedo_cooldown = 0
        self.mine_cooldown = 0

        self.gun_delay = 10
        self.torpedo_delay = 180
        self.mine_delay = 180

        self.start_anim(self.anim_idle())


    def get_rotated_pos(self, x, y):
        """Converts an offset on the base image to approx. coordinates on the rotated image

        Calculation done relative to the center of the image.

        Returns the (x, y)"""

        # convert to polar
        theta = math.atan2(y, x)
        r = math.hypot(x, y)

        # rotate
        theta += self.heading * math.pi / 180

        # convert back to cartesian
        x = r * math.cos(theta)
        y = -r * math.sin(theta)
        return (x, y)

    def handle_input(self, event):
        """Update our state base on all the keyboard input.

        parse_input() uses this flags to actually do the changes.

        Returns True if we consumed the event, False if we were unable to handle it.
        """

        handled = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                self.accelerating = True
            elif event.key == pygame.K_DOWN:
                self.stopping = True
            elif event.key == pygame.K_RIGHT:
                self.diving = True
            elif event.key == pygame.K_LEFT:
                self.climbing = True
            elif event.key == pygame.K_z:
                self.flipped = not self.flipped
            elif event.key == pygame.K_x:
                self.mine = True
            elif event.key == pygame.K_c:
                self.torpedo = True
            elif event.key == pygame.K_SPACE:
                self.gun = True
            elif event.key == pygame.K_v:
                # alias for spacebar
                self.gun = True
            else:
                handled = False

        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_UP:
                self.accelerating = False
            elif event.key == pygame.K_DOWN:
                self.stopping = False
            elif event.key == pygame.K_RIGHT:
                self.diving = False
            elif event.key == pygame.K_LEFT:
                self.climbing = False
            elif event.key == pygame.K_z:
                # only flip on keydown
                pass
            elif event.key == pygame.K_x:
                self.mine = False
            elif event.key == pygame.K_c:
                self.torpedo = False
            elif event.key == pygame.K_SPACE:
                self.gun = False
            elif event.key == pygame.K_v:
                self.gun = False
            else:
                handled = False

        else:
            handled = False

        return handled

    def parse_input(self):
        """update the object based on the input"""

        if self.dead:
            return

        update_needed = False

        # speed
        if self.accelerating:
            self.engine_speed += self.acceleration
            if self.engine_speed > self.max_speed:
                self.engine_speed = self.max_speed
            update_needed = True

        if self.stopping:
            self.engine_speed -= self.braking
            if self.engine_speed < 0:
                self.engine_speed = 0
            update_needed = True

        # heading
        if not self.flipped:
            if self.climbing:
                self.heading += self.climb_rate
                update_needed = True
            if self.diving:
                self.heading -= self.dive_rate
                update_needed = True
        else:
            if self.climbing:
                self.heading -= self.climb_rate
                update_needed = True
            if self.diving:
                self.heading += self.dive_rate
                update_needed = True

        if self.heading > 180:
            self.heading -= 360
        elif self.heading <= -180:
            self.heading += 360

        # update the actual physics
        if update_needed:
            self.set_move_vector(self.heading * math.pi / -180, self.engine_speed)

    def fire_weapons(self, sprites, frame_count):
        """Process weapons fire. Can add new sprites."""
        if self.gun and (self.gun_cooldown <= frame_count):
            self.gun_cooldown = frame_count + self.gun_delay

            # locate the physical position of the weapon
            pos = self.pos
            pos_offset = self.get_rotated_pos(*self.gun_location)
            pos = (pos[0] + pos_offset[0], pos[1] + pos_offset[1])

            sprites.append(sprite_types.TinyBibbleMob(pos))
            sprites.append(sprite_types.BulletMob(pos, self.theta, self.dr, self  ))

        if self.torpedo and (self.torpedo_cooldown <= frame_count):
            self.torpedo_cooldown = frame_count + self.torpedo_delay 
            debug("Fish in the water! %d" % self.torpedo_cooldown)

        if self.mine and (self.mine_cooldown <= frame_count):
            self.mine_cooldown = frame_count + self.mine_delay
            debug("What's mine is yours. %d" % self.mine_cooldown)

    def herd_children(self, sprites):
        """Move our child sprites into the main sprite list"""
        sprites.extend(self.child_sprites)
        self.child_sprites = []

    def anim_rotate(self, force = False):
        """Rotate the canonical image to produce one suitable for our heading

        Returns True if the image changed

        Called from the animation coroutines
        """
        if (not force) and abs(self.heading - self.image_heading) < 2 and (self.image_flipped == self.flipped):
            # ignore small changes since this is quite an expensive operation
            return False

        #debug("Rotating player to %0.6f, flipped = %s" % (self.heading, str(self.flipped)))

        tmp_image = self.image_real

        if self.flipped:
            tmp_image = pygame.transform.flip(tmp_image, False, True)

        self.image = pygame.transform.rotate(tmp_image, self.heading)
        self.image_heading = self.heading
        self.image_flipped = self.flipped

        # maintain the center of our rect
        center = self.rect.center
        self.rect.size = self.image.get_size()
        self.rect.center = center

        return True

    def anim_move(self):
        """Animation coroutine for move animation"""
        frames = self.get_frames("idle")
        force_rotate = True
        image_changed = True

        bubble_interval_max = 440
        bubble_interval = 0
        bubble_counter = 0
        bubble_count = 5

        while True:
            for frame in frames:
                for i in range(13):
                    if (i == 0) and (frame != self.image_real):
                        self.image = frame
                        self.image_real = frame
                        force_rotate = True

                    image_changed = self.anim_rotate(force = force_rotate)
                    force_rotate = False

                    # periodically spawn a bubble
                    bubble_counter += 1
                    if bubble_counter >= bubble_interval:
                        # locate the physical position of the weapon
                        pos = self.pos
                        pos_offset = self.get_rotated_pos(*self.engine_location)
                        pos = (pos[0] + pos_offset[0], pos[1] + pos_offset[1])

                        for i in range(bubble_count):
                            self.child_sprites.append(sprite_types.TinyBibbleMob(pos))

                        bubble_counter = 0
                        bubble_interval = random.triangular(1, bubble_interval_max)
                        if random.random() > 0.2:
                            bubble_count = 1
                        else:
                            bubble_count = 2



                    if self.engine_speed == 0:
                        # full stop!
                        self.start_anim(self.anim_idle())
                        image_changed = True

                    yield image_changed

    def anim_idle(self):
        """Animation coroutine for idle animation"""
        frames = self.get_frames("idle")
        force_rotate = True
        image_changed = True

        while True:
            for frame in frames:
                for i in range(13):
                    if (i == 0) and (frame != self.image_real):
                        self.image = frame
                        self.image_real = frame
                        force_rotate = True

                    image_changed = self.anim_rotate(force = force_rotate)
                    force_rotate = False

                    if self.engine_speed > 0:
                        # start your engines!
                        self.start_anim(self.anim_move())
                        image_changed = True

                    yield image_changed

if __name__ == "__main__":
    import game
    game.main()
