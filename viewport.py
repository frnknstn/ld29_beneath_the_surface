#!/usr/bin/python
from __future__ import division
from __future__ import print_function

import itertools

import pygame

from defines import *

class Viewport(object):
    """Class representing a visible subsection of a level that can be rendered.

    """
    def __init__(self, level, level_pos, surf_pos, size, target=None):
        debug("Creating %dx%d viewport with level '%s':%s at %s)" % (
            size[0], size[1], str(level), str(level_pos), str(surf_pos)))

        self.level = level
        self.size = size
        
        self.target = target        # make the view follow this target

        # set via move_*
        self.surf_pos = (0, 0)      # our offset from the top-left of whatever surface we draw to
        self.tile_pos = (0, 0)      # our position within the level, in tiles
        self.level_pos = (0, 0)     # our position within the level, in pixels
        self.tile_size = (0, 0)     # size of the viewport, measured in the maximum number of tiles needed
        self.tile_mod = (0, 0)      # number of pixels in the final (partial) tile in the row / column
        self.rect = None            # portion of the level we represent

        # prepare us
        self.surf = pygame.Surface(size)
        self.last_update_rect = None    # what portion of the world was drawn in the last call to update()

        self.move_surface(surf_pos)
        self.move_view((level_pos, size))
        self.update()

    def move_view(self, rect):
        """Move and resize this viewport within the level"""
        rect = pygame.Rect(rect)
        rect.clamp_ip(self.level.rect)

        if rect == self.rect:
            return

        # Resize our surface if needed
        if self.size < rect.size:
            new_surf = pygame.Surface(rect.size)
            new_surf.blit(self.surf)
            self.surf = new_surf

        self.rect = rect
        self.size = rect.size
        self.level_pos = rect.topleft



        # x_tile_pos, x_tile_mod = divmod(self.level_pos[0], TILE_SIZE)
        # y_tile_pos, y_tile_mod = divmod(self.level_pos[1], TILE_SIZE)

        # x_tile_size, x_tile_size_mod = divmod(self.size[0], TILE_SIZE)
        # y_tile_size, y_tile_size_mod = divmod(self.size[1], TILE_SIZE)

        # x_tile_mod = (x_tile_mod + x_tile_size_mod) % TILE_SIZE
        # y_tile_mod = (y_tile_mod + y_tile_size_mod) % TILE_SIZE

        # # handle partial tiles
        # if x_tile_mod != 0:
        #     x_tile_size += 1
        # if y_tile_mod != 0:
        #     y_tile_size += 1

        # self.tile_size = (x_tile_size, y_tile_size)
        # self.tile_mod = (x_tile_mod, y_tile_mod)
        # self.tile_pos = (x_tile_pos, y_tile_pos)



        # x_tile_pos, x_tile_pos_mod = divmod(self.level_pos[0], TILE_SIZE)
        # y_tile_pos, y_tile_pos_mod = divmod(self.level_pos[1], TILE_SIZE)

        # x_tile_size, x_tile_size_mod = divmod(self.size[0], TILE_SIZE)
        # y_tile_size, y_tile_size_mod = divmod(self.size[1], TILE_SIZE)

        # # handle partial tiles
        # if x_tile_pos_mod != 0:
        #     x_tile_size += 1
        # if y_tile_pos_mod != 0:
        #     y_tile_size += 1
        # if x_tile_size_mod != 0:
        #     x_tile_size += 1
        # if y_tile_size_mod != 0:
        #     y_tile_size += 1

        # self.tile_size = (x_tile_size, y_tile_size)
        # self.tile_mod = (x_tile_pos_mod, y_tile_pos_mod)
        # self.tile_pos = (x_tile_pos, y_tile_pos)



        x_tile_pos, x_tile_pos_mod = divmod(self.level_pos[0], TILE_SIZE)
        y_tile_pos, y_tile_pos_mod = divmod(self.level_pos[1], TILE_SIZE)

        x_tile_size, x_tile_size_mod = divmod(self.size[0], TILE_SIZE)
        y_tile_size, y_tile_size_mod = divmod(self.size[1], TILE_SIZE)

        # handle partial tiles
        if x_tile_pos_mod != 0:
            x_tile_size += 1
        if y_tile_pos_mod != 0:
            y_tile_size += 1

        self.tile_size = (x_tile_size, y_tile_size)
        self.tile_mod = (x_tile_pos_mod, y_tile_pos_mod)
        self.tile_pos = (x_tile_pos, y_tile_pos)


    def move_view_by(self, x, y):
        """Move this viewport by an (x, y) offset"""
        self.move_view(self.rect.move(x, y))


    def move_surface(self, surf_pos):
        """Move the position of our render on whatever surface we get rendered to"""
        self.surf_pos = surf_pos


    def update_tile(self, xt, yt):
        """Update a single tile on our internal surface render"""
        self.surf.blit(self.level.images[self.level.data[xt][yt]], 
                       ((xt - self.tile_pos[0]) * TILE_SIZE - self.tile_mod[0], 
                        (yt - self.tile_pos[1]) * TILE_SIZE - self.tile_mod[1]))

    def update_full(self):
        """Update the entire internal surface render"""

        if not DRAW_MAP:
            self.surf.fill((0,0,0))
            return

        self.surf.fill((0,0,0))

        data = self.level.data
        images = self.level.images
        surf = self.surf

        # This is based on the way I used to do tile engines in C++.
        # not sure if actually faster in Python...
        x_size, y_size = self.size
        x_tile, y_tile = self.tile_pos

        # deal with partial tiles
        x_level, y_level = self.level_pos
        x_rem, y_rem = self.tile_mod
        x, y = -x_rem, -y_rem

        # draw the tiles    
        x_start, x_start_tile = x, x_tile
        while y < y_size:
            x = x_start
            x_tile = x_start_tile

            while x < x_size:
                # draw the tile
                image = images[data[x_tile][y_tile]]
                surf.blit(image, (x, y))

                # loop
                x += TILE_SIZE
                x_tile += 1

            # loop
            y += TILE_SIZE
            y_tile += 1

        self.last_update_rect = self.rect
        
    def focus_on_target(self):
        """Move the viewport to our target sprite"""
        # calculate the target center based on the floating point pos

        # rect = self.rect.copy()
        # target = self.target
        # target_width, target_height = target.rect.size
        # x_center, y_center = target.pos[0] + (target_width / 2), target.pos[1] + (target_height / 2)
        # rect.center = (x_center, y_center)
        # self.move_view(rect)

        rect = self.rect.copy()
        rect.center = self.target.rect.center
        self.move_view(rect)


    def update(self):
        """Update our internal surface render"""
        
        if self.target is not None:
            self.focus_on_target()
            
        if self.last_update_rect is None:
            # we need to do a complete redraw
            self.update_full()
            return


        self.update_full()
        return
        # The code below used to handle partial redraws of the level, only updating the section
        # that had changed. However, it doesn't work when the size of the viewport is not a multiple
        # of the tile size. We probably need to keep track of the right tile mod or something.
        #
        # I am disabling this code until I can get it fixed. The full update is fast enough for now.

        # # scroll ourself. Only redraw the section that changed.
        # dx = self.last_update_rect.topleft[0] - self.rect.topleft[0]
        # dy = self.last_update_rect.topleft[1] - self.rect.topleft[1]

        # if abs(dx) > TILE_SIZE or abs(dy) > TILE_SIZE:
        #     # more than one row or column needs to be drawn. Just use the default method for now.
        #     self.update_full()
        #     return
            
        # self.surf.scroll(dx, dy)

        # # update the one column / row
        # x_min, y_min = self.tile_pos
        # x_tile_size, y_tile_size =  self.tile_size
        # x_max, y_max = x_min + x_tile_size, y_min + y_tile_size

        # if dx > 0:
        #     # we are going left: scroll the map right, redraw the left
        #     for ty in range(y_min, y_max):
        #         self.update_tile(x_min, ty)
        # elif dx < 0:
        #     # we are going right: scroll the map left, redraw the right
        #     for ty in range(y_min, y_max):
        #         self.update_tile(x_max - 1, ty)

        # if dy > 0:
        #     # we are going up: scroll the map down, redraw the top
        #     for tx in range(x_min, x_max):
        #         self.update_tile(tx, y_min)
        # elif dy < 0:
        #     # we are going down: scroll the map up, redraw the bottom
        #     for tx in range(x_min, x_max):
        #         self.update_tile(tx, y_max - 1)

        # self.last_update_rect = self.rect


    def draw(self, surf, sprites=None, triggers=None):
        """Draw us to an external surface"""

        surf.set_clip((self.surf_pos, self.size))

        # Blit our most recent render to an external surface
        surf.blit(self.surf, self.surf_pos)

        x_offset, y_offset = self.level_pos
        x_offset, y_offset = x_offset - self.surf_pos[0], y_offset - self.surf_pos[1]

        # Draw our sprites:
        if sprites is not None:
            for index in self.rect.collidelistall(sprites):
                pygame.INFO_SPRITE_COUNT += 1
                sprite = sprites[index]
                x, y = sprite.rect.topleft
                x, y = int(x) - x_offset, int(y) - y_offset
                surf.blit(sprite.image, (x, y))

        # Draw our triggers, for debug purposes:
        if triggers is not None:
            for trigger in [ triggers[x] for x in self.rect.collidelistall(triggers) ]:
                pygame.INFO_SPRITE_COUNT += 1
                image = self.level.images[trigger.gid]
                x, y = trigger.x, trigger.y
                x, y = int(x) - x_offset, int(y) - y_offset
                surf.blit(image, (x, y))




        surf.set_clip(None)




if __name__ == "__main__":
    import game
    game.main()
